# HomeDashboard

This is a dashboard to keep track of some interesting information.

# Architecture

The app consists of three sub applications currently:

- The frontend will contain the public facing application
- The widget app is the brain of the app: it will configure the other umbrella apps with settings such as: location, refresh interval etc.
- The traffic app contains all logic to retrieve the traffic status

