defmodule TrafficStatus.Models.RoadTest do
  use ExUnit.Case
  alias TrafficStatus.Models.{Road, SpeedCheck}

  doctest Road

  setup do
    with { :ok, body } <- File.read(File.cwd! <> "/test/stubs/road_entry.json"),
         { :ok, json } <- Poison.decode(body) do
      { :ok, traffic_data: json }
    else
      e -> 
        :error
    end
  end

  test "parses speed checks", %{ traffic_data: road } do
    expected = %SpeedCheck{
      location: "A1 Amersfoort richting Amsterdam",
      description: "Snelheidscontrole tussen afrit Bunschoten en Eembrugge bij hectometerpaal 35,4."
    }
    road = Road.new(road) 
    %{speed_checks: [speed_check]} = road
    %{ location: location, description: description } = speed_check

    assert location == expected.location
    assert description == expected.description
  end

  test "saves the road", %{ traffic_data: road } do
    assert %{ road: "A1" } = Road.new(road)
  end
end
