defmodule TrafficStatus.Repositories.RoadTest do
  use ExUnit.Case
  alias TrafficStatus.Repositories.{Road}

  doctest Road 

  import Mox

  setup do
    with { :ok, body } <- File.read(File.cwd! <> "/test/consumer_contracts/traffic_info.json") do
      { :ok, traffic_data: body }
    else
      _ -> :error
    end
  end


  describe "#list" do
    test "returns the traffic data", %{ traffic_data: traffic_data } do
      TrafficStatus.Repositories.Http.TrafficMock
      |> expect(:list, fn -> { :ok, traffic_data } end)
      assert { :ok, result } = Road.list
      expected = [
        %TrafficStatus.Models.Road{
         road: "A1",
         speed_checks: [
           %TrafficStatus.Models.SpeedCheck{
             description: "Snelheidscontrole tussen afrit Bunschoten en Eembrugge bij hectometerpaal 35,4.",
             location: "A1 Amersfoort richting Amsterdam"
           }
         ]
       },
       %TrafficStatus.Models.Road{ road: "A6", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "A12", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "A15", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "A16", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "A17", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "A73", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "N57", speed_checks: [] },
       %TrafficStatus.Models.Road{ road: "N301", speed_checks: [] }
     ]
      assert result == expected
    end
  end
end

