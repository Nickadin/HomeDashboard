defmodule TrafficStatus.Util.StringUtil do
  def downcase_first(val) when is_binary(val) do
    << first_letter :: binary-size(1), rest :: binary >> = val
    String.downcase(first_letter) <> rest
  end

  def upcase_first(val) when is_binary(val) do
    << first_letter :: binary-size(1), rest :: binary >> = val
    String.upcase(first_letter) <> rest
  end
end

