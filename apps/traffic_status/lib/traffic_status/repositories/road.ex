defmodule TrafficStatus.Repositories.Road do
  alias TrafficStatus.Models.Road
  require Logger

  @traffic_api Application.get_env(:traffic_status, :traffic_api)

  def list do
    with { :ok, body } <- @traffic_api.list,
         { :ok, traffic_data } <- Poison.decode(body)
    do
      %{ "roadEntries" => entries } = traffic_data
      { :ok, Enum.map(entries, &Road.new/1) }
    else
      error ->
        Logger.error(inspect error)
        :error
    end
  end
end

