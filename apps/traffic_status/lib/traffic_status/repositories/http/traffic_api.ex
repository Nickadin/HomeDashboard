defmodule TrafficStatus.Repositories.Http.TrafficApi do
  @behavior TrafficStatus.Repositories.Http.TrafficHttp
  require Logger

  def list do
    case HTTPoison.get("https://www.anwb.nl/feeds/gethf") do
      { :ok, %{ status_code: 200, body: body } } -> { :ok, body }
      { :ok, %{ status_code: status_code, body: body } } ->
        Logger.error(fn ->
          "Request failed with status_code #{inspect status_code} and body #{inspect body}"
        end)
        { :error, status_code }
      e ->
        Logger.error(inspect e)
        { :error, :internal }
    end
  end
end
