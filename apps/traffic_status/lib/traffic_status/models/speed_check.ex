defmodule TrafficStatus.Models.SpeedCheck do
  defstruct [:location, :description]

  alias TrafficStatus.Util.StringUtil

  @type input :: %{ description: String.t, location: String.t, events: [%{ type: String.t }] }

  @doc """
  Parses traffic information and converts it to a speed check
  """
  @spec new(input) :: __MODULE__
  def new(%{ "description" => description, "location" => location, "events" => [event] }) do
    %{ "text" => type } = event
    %__MODULE__{
      location: location,
      description: to_sentence([type, description])
    }
  end

  defp to_sentence(args) when is_list(args) do
    args
      |> Enum.map(&StringUtil.downcase_first/1)
      |> Enum.join(" ")
      |> StringUtil.upcase_first
  end
end
