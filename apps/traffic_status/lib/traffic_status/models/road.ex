defmodule TrafficStatus.Models.Road do
  defstruct [:road, :speed_checks]

  alias TrafficStatus.Models.SpeedCheck

  @type input :: %{ road: String.t, events: List.t }

  @doc """
  Parses traffic info to create a Road model, containing all information about a road
  """
  @spec new(input) :: __MODULE__
  def new(%{ "road" => road, "events" => events }) do
    %{ "radars" => radars } = events
    %__MODULE__{
      road: road,
      speed_checks: Enum.map(radars, &SpeedCheck.new/1)
    }
  end
end

