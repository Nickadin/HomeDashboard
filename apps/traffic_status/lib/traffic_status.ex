defmodule TrafficStatus do
  @moduledoc """
  The public API to fetch traffic statussen
  """

  @doc """
  Requests all current traffic info
  """
  def list do
    TrafficStatus.Repositories.Road.list
  end
end
