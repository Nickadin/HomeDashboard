defmodule Widgets do
  @moduledoc """
  Documentation for Widgets.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Widgets.hello
      :world

  """
  def hello do
    :world
  end
end
